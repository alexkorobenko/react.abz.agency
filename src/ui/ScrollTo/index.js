import React from "react";
import { Button } from "@/ui";
import scrollToTarget from "@/utils/scrollToTarget";

const onClickHandler = e => {
  const currentTarget = e.currentTarget;
  const href = currentTarget.getAttribute("data-href");

  scrollToTarget(href);
};

const ScrollTo = props => {
  return (
    <Button {...props}
            onClick={onClickHandler}>
      {props.children && props.children}
    </Button>
  );
};

export default ScrollTo;