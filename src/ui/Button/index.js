import React from "react";
import PropTypes from "prop-types";

const Button = ({ type, dataHref, title, className, disabled, onClick, children }) => {
  return (
    <button type={type}
            data-href={dataHref}
            className={`button ${className}`}
            disabled={disabled}
            onClick={onClick}>
      {children ?
        children :
        <span>{title}</span>
      }
    </button>
  );
};

Button.propTypes = {
  type: PropTypes.string,
  dataHref: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  type: "button",
  disabled: false,
};

export default Button;