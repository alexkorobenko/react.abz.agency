import React from "react";
import InputMask from "react-input-mask";
import PropTypes from "prop-types";
import classNames from 'classnames';
import styles from "./index.module.scss";

const FormInput = ({ name, label, type, value, valid, touched, placeholder, errorMessage, mask, autoComplete, onChange }) => {
  const InputTagName = mask ? InputMask : "input";
  let options = {
    "type": type,
    "name": name,
    "value": value,
    "placeholder": placeholder,
    "autoComplete": autoComplete,
  };

  if (mask) {
    options["mask"] = mask;
    options["alwaysShowMask"] = true;
  }

  return (
    <div className={classNames(styles.row, {
      [styles.error]: touched && !valid,
    })}>
      <div className={styles.field}>
        {label &&
        <label htmlFor={name}>{label}</label>
        }

        <InputTagName {...options}
                      onChange={onChange} />
      </div>

      {(touched && !valid && errorMessage) &&
      <div className={styles.message}>
        <span>{errorMessage}</span>
      </div>
      }
    </div>
  );
};

FormInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  valid: PropTypes.bool,
  touched: PropTypes.bool,
  placeholder: PropTypes.string,
  errorMessage: PropTypes.string,
  mask: PropTypes.string,
  autoComplete: PropTypes.string,
  onChange: PropTypes.func,
};

FormInput.defaultProps = {
  type: "text",
  value: "",
  placeholder: "",
  autoComplete: "off",
  valid: false,
  touched: false,
  errorMessage: "",
};

export default FormInput;