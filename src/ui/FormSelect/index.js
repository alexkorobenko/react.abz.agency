import React, {useEffect, useRef, useState} from "react";
import PropTypes from "prop-types";
import { initEventPathPolyfill } from "@/utils/helpers";
import classNames from 'classnames';
import styles from "./index.module.scss";

const FormSelect = ({ name, value, items, valid, touched, placeholder, onChange }) => {
  const ref = useRef();
  const [visible, setVisible] = useState(false);

  const toggle = () => setVisible(prevState => !prevState);

  const onClickHandler = value => {
    setVisible(false);

    onChange && onChange({
      name: name,
      value: value,
    });
  };

  const onDocumentClickHandler = e => {
    initEventPathPolyfill();

    if (e.path.indexOf(ref.current) === -1) {
      setVisible(false);
    }
  };

  const getTitleByValue = value => {
    const element = items.find(el => parseInt(el.id) === parseInt(value));

    return element ? element["name"] : placeholder;
  };

  useEffect(() => {
    document.addEventListener("click", onDocumentClickHandler);

    return () => {
      document.removeEventListener("click", onDocumentClickHandler);
    }
  }, []);

  return (
    <div ref={ref}
         className={classNames(styles.select, {
           [styles.visible]: visible,
           [styles.error]: touched && !valid,
         })}>
      <div className={styles.header}
           onClick={toggle}>
        <div className={styles.title}>{getTitleByValue(value)}</div>
        <div className={styles.icon}>
          <svg width="16" height="9" viewBox="0 0 16 9" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M15.7 0.300009C15.5194 0.105732 15.2652 -0.00322417 15 8.92433e-06H0.999994C0.734786 -0.00322417 0.480554 0.105732 0.299994 0.300009C0.111579 0.484639 0.00375071 0.73624 -6.33446e-06 1.00001C-0.00323943 1.26522 0.105717 1.51945 0.299994 1.70001L7.29999 8.70001C7.68884 9.08115 8.31115 9.08115 8.69999 8.70001L15.7 1.70001C15.8943 1.51945 16.0032 1.26522 16 1.00001C15.9962 0.73624 15.8884 0.484639 15.7 0.300009Z" fill="black"/>
          </svg>
        </div>
      </div>
      <div className={styles.body}>
        {items &&
        <ul className={styles.list}>
          {
            items.map((el, key)=>(
              <li key={key}
                  onClick={() => onClickHandler(el.id)}>{el.name}</li>
            ))
          }
        </ul>}
      </div>
      <input type="hidden"
             name={name}
             value={value}
             autoComplete="off" />
    </div>
  );
};

FormSelect.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  items: PropTypes.array,
  valid: PropTypes.bool,
  touched: PropTypes.bool,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
};

FormSelect.defaultProps = {
  items: [],
  placeholder: "Chose item",
  valid: false,
  touched: false,
};

export default FormSelect;