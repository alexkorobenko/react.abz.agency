import React from "react";
import PropTypes from "prop-types";

const Link = ({ title, className, target, href, children }) => {
  return (
    <a className={className}
       href={href}
       target={target}>
      {children ?
        children :
        <span>{title}</span>
      }
    </a>
  );
};

Link.propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  target: PropTypes.string,
  href: PropTypes.string.isRequired,
};

Link.defaultProps = {
  target: "_self",
};

export default Link;