import React, { createRef, useEffect } from "react";
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock/lib/bodyScrollLock.es6";
import { Button } from "@/ui";
import PropTypes from "prop-types";
import classNames from 'classnames';
import styles from "./index.module.scss";

const Notification = ({ title, message, visible, onConfirm }) => {
  const ref = createRef();

  const toggle = visible => {
    visible ? disableBodyScroll(ref.current) : enableBodyScroll(ref.current);
  };

  const confirm = () => {
    toggle(false);
    onConfirm && onConfirm();
  };

  useEffect(() => {
    toggle(visible);
  }, [visible]); // eslint-disable-line

  return (
    <div ref={ref}
         className={classNames(styles.notification, {
           [styles.visible]: visible,
         })}>
      <div className={styles.mask}>
      </div>
      <div className={styles.wrapper}>
        <div className={styles.header}>
          <div className={styles.title}>{title}</div>
        </div>
        <div className={styles.body}>
          <div className={styles.message} dangerouslySetInnerHTML={{__html: message}}>
          </div>
        </div>
        <div className={styles.footer}>
          <Button title="OK"
                  onClick={confirm}
                  className="button--text" />
        </div>
      </div>
    </div>
  );
};

Notification.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  visible: PropTypes.bool,
  onConfirm: PropTypes.func,
};

Notification.defaultProps = {
  visible: false,
};

export default Notification;