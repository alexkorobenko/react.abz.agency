const homeMenu = [
  {
    "title": "About me",
    "action": "scrollTo",
    "href": "#about-me"
  },
  {
    "title": "Relationships",
    "action": "scrollTo",
    "href": "#relationships"
  },
  {
    "title": "Requirements",
    "action": "scrollTo",
    "href": "#requirements"
  },
  {
    "title": "Users",
    "action": "scrollTo",
    "href": "#users"
  },
  {
    "title": "Sign Up",
    "action": "scrollTo",
    "href": "#sign-up"
  },
  {
    "title": "Sign Out",
    "href": "/sign-out"
  },
];

const siteMenu = [
  {
    "title": "News",
    "href": "/"
  },
  {
    "title": "Overview",
    "href": "/;"
  },
  {
    "title": "Tutorials",
    "href": "/;"
  },
  {
    "title": "FAQ",
    "href": "/;"
  },
  {
    "title": "Blog",
    "href": "/;"
  },
  {
    "title": "Design",
    "href": "/;"
  },
  {
    "title": "Resources",
    "href": "/;"
  },
  {
    "title": "Terms",
    "href": "/;"
  },

  {
    "title": "Partners",
    "href": "/;"
  },
  {
    "title": "Code",
    "href": "/;"
  },
  {
    "title": "Guides",
    "href": "/;"
  },
  {
    "title": "Conditions",
    "href": "/;"
  },

  {
    "title": "Shop",
    "href": "/;"
  },
  {
    "title": "Collaborate",
    "href": "/;"
  },
  {
    "title": "Examples",
    "href": "/;"
  },
  {
    "title": "Help",
    "href": "/;"
  },
];

const signUpFormFields = {
  name: {
    type: "input",
    priority: 1,
    pattern: /^[^\d+=()[\]{}\\/^$|?*!@#%:;&,_.]{2,60}$/,
    options: {
      label: "Name",
      value: "",
      valid: false,
      touched: false,
      placeholder: "Your name",
      errorMessage: "should be 2-60 characters",
    },
  },
  email: {
    type: "input",
    priority: 2,
    pattern: /[A-Za-z0-9._%+-]+@[A-Za-z0-9\-.]+\.[A-Za-z]{2,4}$/,
    options: {
      label: "Email",
      type: "email",
      value: "",
      valid: false,
      touched: false,
      placeholder: "Your email",
      errorMessage: "incorrect email",
    },
  },
  phone: {
    type: "input",
    priority: 3,
    pattern: /[+( )0-9]{10,19}$/,
    options: {
      label: "Phone",
      type: "tel",
      value: "",
      valid: false,
      touched: false,
      errorMessage: "incorrect phone",
      mask: "+38 (999) 999 99 99",
    },
  },
  position_id: {
    type: "select",
    pattern: /[0-9]$/,
    options: {
      value: "",
      valid: false,
      touched: false,
      placeholder: "Select your position",
    },
  },
  photo: {
    type: "file",
    options: {
      valid: false,
      touched: false,
      accept: "image/jpeg",
      maxSize: 5,
      minWidth: 70,
      minHeight: 70,
      placeholder: "Upload your photo",
      button: {
        title: "Upload",
      },
    }
  },
};

export default {
  "profile": {
    "id": 1,
  },
  "homeMenu": homeMenu,
  "siteMenu": siteMenu,
  "banner": {
    "bg": {
      "xl": "images/Banner/bg-1-xl.jpg",
      "lg": "images/Banner/bg-1-lg.jpg",
      "md": "images/Banner/bg-1-md.jpg",
      "sm": "images/Banner/bg-1-sm.jpg",
      "xs": "images/Banner/bg-1-xs.jpg",
    },
    "title": "Test assignment for Frontend Developer position",
    "description": "<p>We kindly remind you that your test assignment should be submitted as a link to github/bitbucket repository.</p><p>Please be patient, we consider and respond to every application that meets minimum requirements. We look forward to your submission. Good luck!</p>",
    "button": {
      "title": "Sign Up",
      "dataHref": "#sign-up",
      "className": "button--orange",
    },
  },
  "about": {
    "image": "image.svg",
    "title": "Let's get ac quainted",
    "description": "<h3>I am cool frontend developer</h3><p>When real users have a slow experience on mobile, they're much less likely to find what they are looking for or purchase from you in the future. For many sites this equates to a huge missed opportunity, especially when more than half of visits are abandoned if a mobile page takes over 3 seconds to load.</p><p>Last week, Google Search and Ads teams announced two new speed initiatives to help improve user-experience on the web. </p>",
    "button": {
      "title": "Sign Up",
      "dataHref": "#sign-up",
      "className": "button--text",
    },
  },
  "relationships": {
    "title": "About my relationships with web-development",
    "items": [
      {
        "image": "html.svg",
        "title": "I'm in love with HTML",
        "description": "<p>Hypertext Markup Language (HTML) is the standard markup language for creating web pages and web applications.</p>"
      },
      {
        "image": "css.svg",
        "title": "CSS is my best friend",
        "description": "<p>Cascading Style Sheets (CSS) is a  style sheet language used for describing the presentation of a document written in a markup language like HTML.</p>"
      },
      {
        "image": "javascript.svg",
        "title": "JavaScript is my passion",
        "description": "<p>JavaScript is a high-level, interpreted programming language. It is a language which is also characterized as dynamic, weakly typed, prototype-based and multi-paradigm.</p>"
      },
    ]
  },
  "requirements": {
    "image": "man-laptop-v1.svg",
    "bg": {
      "xl": "images/Requirements/bg-1-xl.jpg",
      "lg": "images/Requirements/bg-1-lg.jpg",
      "md": "images/Requirements/bg-1-md.jpg",
      "sm": "images/Requirements/bg-1-sm.jpg",
      "xs": "images/Requirements/bg-1-xs.jpg",
    },
    "title": "General requirements for the test task",
    "description": "<p>Users want to find answers to their questions quickly and data shows that people really care about how quickly their pages load. The Search team announced speed would be a ranking signal for desktop searches in 2010 and as of this month (July 2018), page speed will be a ranking factor for mobile searches too.</p><p>If you're a developer working on a site, now is a good time to evaluate your performance using our speed tools. Think about how performance affects the user experience of your pages and consider measuring a variety of real-world user-centric performance metrics.</p><p>Are you shipping too much JavaScript? Too many images? Images and JavaScript are the most significant contributors to the page weight that affect page load time based on data from HTTP Archive and the Chrome User Experience Report - our public dataset for key UX metrics as experienced by Chrome users under real-world conditions.</p>",
  },
  "users": {
    "title": "Our cheerful users",
    "subTitle": "Attention! Sorting users by registration date",
    "button": {
      "title": "Show more",
      "className": "button--border",

    },
  },
  "signUp": {
    "title": "Register to get a work",
    "subTitle": "Attention! After successful registration and alert, update the list of users in the block from the top",
    "fields": signUpFormFields,
    "button": {
      "title": "Sign Up",
      "className": "button--orange",
    },
  },
  "footer": {
    "homeMenu": homeMenu,
    "siteMenu": siteMenu,
    "contact": [
      {
        "type": "mail",
        "href": "work.of.future@gmail.com",
        "title": "work.of.future@gmail.com",
      },
      {
        "type": "phone",
        "href": "+38(044)7892498",
        "title": "+38 (044) 789 24 98",
      },
      {
        "type": "mobile",
        "href": "+38(095)5560845",
        "title": "+38 (095) 556 08 45",
      },
    ],
    "social": [
      {
        "href": "/;",
        "type": "fb"
      },
      {
        "href": "/;",
        "type": "ln"
      },
      {
        "href": "/;",
        "type": "in"
      },
      {
        "href": "/;",
        "type": "tw"
      },
      {
        "href": "/;",
        "type": "pin"
      },
    ],
    "copyright": "&copy; abz.agency specially for the test task",
  },
}