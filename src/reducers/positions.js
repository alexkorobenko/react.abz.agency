import * as constants from "@/constants/action-types";

const initialState = {
  loading: false,
  data:    [],
  error:   null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.GET_POSITIONS_START:
      return {
        ...state,
        loading: true,
      };

    case constants.GET_POSITIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        error:   null,
        data:    [...state.data, ...action.payload.positions],
      };

    case constants.GET_POSITIONS_ERROR:
      return {
        ...state,
        loading: false,
        error:   action.payload.message,
      };

    default:
      return state;
  }
};