import * as constants from "@/constants/action-types";

const initialState = {
  loading: false,
  token:   null,
  error:   null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.REFRESH_TOKEN_START:
      return {
        ...state,
        loading: true,
      };

    case constants.REFRESH_TOKEN_SUCCESS:
      const { token } = action.payload;
      window.localStorage.setItem("token", token);

      return {
        ...state,
        loading: false,
        error:   null,
        token:   token,
      };

    case constants.REFRESH_TOKEN_ERROR:
      return {
        ...state,
        loading: false,
        error:   action.payload.message,
      };

    default:
      return  state;
  }
};