import * as constants from "@/constants/action-types";

const initialState = {
  loading: false,
  nextUrl: null,
  items:   [],
  error:   null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.GET_USERS_START:
      return {
        ...state,
        loading: true,
      };

    case constants.GET_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        error:   null,
        nextUrl: action.payload.links.next_url,
        items:    action.payload.page === 1 ? [...action.payload.users] : [...state.items, ...action.payload.users],
      };

    case constants.GET_USERS_ERROR:
      return {
        ...state,
        loading: false,
        error:   action.payload.message,
      };

    default:
      return state;
  }
};