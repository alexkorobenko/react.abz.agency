import * as constants from "@/constants/action-types";

const initialState = {
  loading: false,
  userId:  null,
  message: null,
  fails:   null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.SIGN_UP_USER_START:
      return {
        ...state,
        loading: true,
      };

    case constants.SIGN_UP_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        userId:  action.payload.user_id,
        message: action.payload.message,
        fails:   null,
      };

    case constants.SIGN_UP_USER_ERROR:
      return {
        ...state,
        loading: false,
        userId:  null,
        message: action.payload.message,
        fails:   action.payload.fails || null,
      };

    default:
      return state;
  }
};