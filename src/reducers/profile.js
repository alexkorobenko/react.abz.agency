import * as constants from "@/constants/action-types";

const initialState = {
  loading: false,
  data:    {},
  error:   null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.GET_PROFILE_START:
      return {
        ...state,
        loading: true,
      };

    case constants.GET_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        error:   null,
        data:    Object.assign({}, state.data, action.payload.user),
      };

    case constants.GET_PROFILE_ERROR:
      return {
        ...state,
        loading: false,
        error:   action.payload.message,
      };

    default:
      return state;
  }
};