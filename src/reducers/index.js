import { combineReducers } from "redux";
import auth from "./auth";
import positions from "./positions";
import profile from "./profile";
import signUp from "./signUp";
import users from "./users";

const rootReducer = combineReducers({
  auth,
  positions,
  profile,
  signUp,
  users,
});

export default rootReducer;