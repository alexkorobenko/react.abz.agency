import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import User from "./User";
import { Header, Banner, About, Relationships, Requirements, Users, SignUp, Footer } from "./components";
import { refreshToken, getProfile, getPositions } from "./actions";
import { debounce, detectMobile } from "@/utils/helpers";
import EventObserver from "@/utils/EventObserver";

const isMobile = detectMobile();
const resizeEventName = isMobile ? "orientationchange" : "resize";

window["responsiveImagesObserver"] = new EventObserver();

const onResizeHandler = () => {
  window["responsiveImagesObserver"].broadcast();
};

function App({ profile, homeMenu, banner, about, relationships, requirements, users, signUp, footer }) {
  const dispatch = useDispatch();

  const onRefreshToken = callback => {
    dispatch(
      refreshToken()
    ).then(() => {
      (callback && callback.success) && callback.success();
    }, () => {
      (callback && callback.error) && callback.error();
    });
  };

  const onGetProfile = id => dispatch(getProfile(id));

  const onGetPositions = () => dispatch(getPositions());

  useEffect(() => {
    const user = new User(profile.id);

    //rewrite super method
    user.refreshToken = () => onRefreshToken({
      success: () => {
        user.updateTokenExpiresTime();
        user.update();
      }
    });
    user.getProfile   = () => onGetProfile(user.id);
    user.getPositions = () => onGetPositions();

    //init
    user.init();

    //resize event for responsive images
    window.addEventListener(resizeEventName, debounce(onResizeHandler, 300));

    return () => {
      window.removeEventListener(resizeEventName, debounce(onResizeHandler, 300));
    };
  }, []); // eslint-disable-line

  return (
    <div id="app">
      <Header menu={homeMenu} />
      <main>
        <div className="main-wrapper">
          <Banner {...banner} />
          <About {...about} />
          <Relationships {...relationships} />
          <Requirements {...requirements} />
          <Users {...users} />
          <SignUp {...signUp} />
        </div>
      </main>
      <Footer {...footer} />
    </div>
  );
}

export default App;