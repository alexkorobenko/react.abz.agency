class User {
  constructor(id) {
    this.id = id;
    this.tokenLifeTime = 1000 * 60 * 40; //40 minutes
  };

  init() {
    this.getProfile();
    this.getPositions();
    this.update();
  };

  update() {
    const timeNow = new Date().getTime();
    const tokenExpiresTime = window.localStorage.getItem("tokenExpiresTime") * 1;
    const tokenDelayTime = tokenExpiresTime - timeNow > 0 ? tokenExpiresTime - timeNow : 0;

    setTimeout(() => {
      this.refreshToken();
    }, tokenDelayTime);
  };

  getProfile() {
    console.log("getProfile");
  };

  getPositions() {
    console.log("getPositions");
  };

  refreshToken() {
    console.log("refreshToken");
  };

  updateTokenExpiresTime() {
    const timeNow = new Date().getTime();
    const tokenExpiresTime = timeNow + this.tokenLifeTime;
    window.localStorage.setItem("tokenExpiresTime", tokenExpiresTime.toString());
  };
}

export default User;