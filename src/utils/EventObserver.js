export default class EventObserver {
  constructor() {
    this.observers = [];
  }

  subscribe(observer) {
    if (this.observers.indexOf(observer) !== -1) return;
    this.observers.push(observer);
  }

  unsubscribe(observer) {
    this.observers = this.observers.filter(_observer => _observer !== observer)
  }

  broadcast(data) {
    this.observers.forEach(_observer => (_observer && _observer.update) && _observer.update(data))
  }
}