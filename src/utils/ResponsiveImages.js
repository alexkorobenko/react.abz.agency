import { foreachObj } from "./helpers";

export default class ResponsiveImages {
  constructor(sectionNode) {
    this.sectionNode = sectionNode;
    this.currentSize = null;
    this.imagesData = {};
    this.config = {
      "sizes": {
        "xs": 1,
        "sm": 568,
        "md": 992,
        "lg": 1280,
        "xl": 1920
      }
    };

    this.init();
    this.update();
  }

  init() {
    const images = Array.from(this.sectionNode.getElementsByClassName("js-responsive-image"));

    images.forEach((image, i) => {
      this.imagesData[i] = {
        "node": image,
        "tagName": image.tagName,
        "breakpoints": {}
      };

      foreachObj(this.config.sizes, (sizeValue, sizeKey) => {
        const src = image.getAttribute(`data-${sizeKey}-src`);
        src && (this.imagesData[i]["breakpoints"][sizeKey] = src);
      });
    });
  }

  update() {
    let size = null;

    foreachObj(this.config.sizes, (sizeValue, sizeKey) => {
      (this.sectionNode.offsetWidth >= sizeValue) && (size = sizeKey);
    });

    if (size !== this.currentSize) {
      this.currentSize = size;
      this.render();
    }
  }

  render() {
    foreachObj(this.imagesData, data => {
      let { [Object.keys(data.breakpoints).pop()] : src } = data.breakpoints; //get last element from data.breakpoints

      foreachObj(data.breakpoints, (breakpointValue, breakpointKey) => {
        (breakpointKey === this.currentSize) && (src = breakpointValue);
      });

      if (data.tagName === "IMG") {
        data.node.src = src;
      } else {
        data.node.style.backgroundImage = `url(${src})`;
      }
    });
  }
}