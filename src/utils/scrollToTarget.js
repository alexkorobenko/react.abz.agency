import smoothScrollTo from "@/utils/smoothScrollTo";

export default (target, speed, callbacks, offset) => {
  if (typeof speed === "undefined") speed = 1500;

  let scroll = function (y) {
    smoothScrollTo(0, y, speed, {
      "onComplete": function () {
        if (callbacks && callbacks.onComplete) callbacks.onComplete();
      }
    });
  };

  if (!target) {
    return true;
  }

  let node = null;

  if (typeof target === "string") {
    node = document.querySelector(target);
  } else {
    node = target;
  }

  if (!node) return false;

  let top =  (window.pageYOffset || document.documentElement.scrollTop) + node.getBoundingClientRect().top;
  let header = document.querySelector(".header");

  if (header)
    top -= header.offsetHeight;

  if (offset)
    top += offset;

  scroll(top);
};