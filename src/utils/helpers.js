export function detectMobile() {
  let userAgents = ['android','astel','audiovox','blackberry','chtml','docomo','ericsson','hand','iphone ','ipod','2me','ava','j-phone','kddi','lg','midp','mini','minimo','mobi','mobile','mobileexplorer','mot-e','motorola','mot-v','netfront','nokia', 'palm','palmos','palmsource','pda','pdxgw','phone','plucker','portable','portalmmm','sagem','samsung','sanyo','sgh','sharp','sie-m','sie-s','smartphone','softbank','sprint','symbian','telit','tsm','vodafone','wap','windowsce','wml','xiino'];
  let agt = navigator.userAgent.toLowerCase();
  let isMobile = false;

  for( let i = 0; i < userAgents.length; i+=1 ){
    if (agt.indexOf(userAgents[i]) !== -1) {
      isMobile = true;
      break;
    }
  }

  if (isMobile) {
    document.documentElement.classList.add("is-mobile");
  } else {
    document.documentElement.classList.add("is-desktop");
  }

  return isMobile;
}

export function debounce(func, wait, immediate) {
  let timeout;

  return function() {
    let context = this, args = arguments;
    let later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    let callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

export function getBase64FromFile(file) {
  const reader = new FileReader();

  return new Promise((resolve, reject) => {
    reader.readAsDataURL(file);
    reader.onload  = () => resolve(reader.result);
    reader.onerror = () => reject(null);
  });
}

export function doConvertBase64ToImage(base64) {
  return new Promise( (resolve, reject) => {
    const image = new Image();

    image.onload  = () => resolve(image);
    image.onerror = () => reject(null);
    image.src = base64;
  });
}

export function doUploadImage(file, options) {
  return new Promise((resolve, reject) => {
    if (!file) {
      return reject("File is undefined");
    }

    const {maxSize, minWidth, minHeight} = options;

    if (file.size > maxSize * 1024 * 1024) {
      return reject(`File size is must be maximum ${maxSize} MB`);
    }

    getBase64FromFile(file)
      .then(doConvertBase64ToImage)
      .then(image => {
        const naturalWidth = image.naturalWidth;
        const naturalHeight = image.naturalHeight;

        if (naturalWidth < minWidth || naturalHeight < minHeight) {
          reject(`Image size is must be minimum as ${minWidth}x${minHeight} px`);
        } else {
          resolve(file);
        }
      });
  });
}

export function foreachObj(obj, callback) {
  if (!obj || !Object.keys(obj).length || !callback) return;

  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      callback(obj[key], key, obj);
    }
  }
}

export function initEventPathPolyfill() {
  if (!("path" in Event.prototype))
    Object.defineProperty(Event.prototype, "path", {
      get: function() {
        let path = [];
        let currentElem = this.target;

        while (currentElem) {
          path.push(currentElem);
          currentElem = currentElem.parentElement;
        }

        if (path.indexOf(window) === -1 && path.indexOf(document) === -1)
          path.push(document);

        if (path.indexOf(window) === -1)
          path.push(window);

        return path;
      }
    });
}