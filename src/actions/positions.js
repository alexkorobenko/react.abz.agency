import {
  GET_POSITIONS_START,
  GET_POSITIONS_SUCCESS,
  GET_POSITIONS_ERROR,
} from "@/constants/action-types";

import api from "@/api/positions";

export const getPositions = () => {
  return dispatch => {
    dispatch(getPositionsStart());

    api.getPositions()
      .then(response => {
        dispatch(getPositionsSuccess(response));
      }, e => {
        dispatch(getPositionsError(e));
      });
  }
};

export const getPositionsStart = payload => {
  return { type: GET_POSITIONS_START, payload }
};

export const getPositionsSuccess = payload => {
  return { type: GET_POSITIONS_SUCCESS, payload }
};

export const getPositionsError = payload => {
  return { type: GET_POSITIONS_ERROR, payload }
};

export default getPositions;