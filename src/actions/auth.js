import {
  REFRESH_TOKEN_START,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_ERROR
} from "@/constants/action-types";

import api from "@/api/auth";

export const refreshToken = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(refreshTokenStart());

      api
        .refreshToken()
        .then(response => {
          dispatch(refreshTokenSuccess(response));
          resolve();
        }, e => {
          dispatch(refreshTokenError(e));
          reject();
        });
    });
  }
};

export const refreshTokenStart = payload => {
  return { type: REFRESH_TOKEN_START, payload }
};

export const refreshTokenSuccess = payload => {
  return { type: REFRESH_TOKEN_SUCCESS, payload }
};

export const refreshTokenError = payload => {
  return { type: REFRESH_TOKEN_ERROR, payload }
};

export default refreshToken;