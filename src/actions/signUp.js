import {
  SIGN_UP_USER_START,
  SIGN_UP_USER_SUCCESS,
  SIGN_UP_USER_ERROR,
} from "@/constants/action-types";

import api from "@/api/signUp";

export const signUpUser = payload => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(signUpUserStart());

      api.signUpUser(payload)
        .then(response => {
          dispatch(signUpUserSuccess(response));
          resolve();
        }, e => {
          dispatch(signUpUserError(e));
          reject();
        });
    });
  }
};

export const signUpUserStart = payload => {
  return { type: SIGN_UP_USER_START, payload }
};

export const signUpUserSuccess = payload => {
  return { type: SIGN_UP_USER_SUCCESS, payload }
};

export const signUpUserError = payload => {
  return { type: SIGN_UP_USER_ERROR, payload }
};

export default signUpUser;