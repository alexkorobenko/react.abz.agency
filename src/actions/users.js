import {
  GET_USERS_START,
  GET_USERS_SUCCESS,
  GET_USERS_ERROR,
} from "@/constants/action-types";

import api from "@/api/users";

export const getUsers = url => {
  return dispatch => {
    dispatch(getUsersStart());

    api.getUsers(url)
      .then(response => {
        dispatch(getUsersSuccess(response));
      }, e => {
        dispatch(getUsersError(e));
      });
  }
};

export const getUsersStart = payload => {
  return { type: GET_USERS_START, payload }
};

export const getUsersSuccess = payload => {
  return { type: GET_USERS_SUCCESS, payload }
};

export const getUsersError = payload => {
  return { type: GET_USERS_ERROR, payload }
};

export default getUsers;