import {
  GET_PROFILE_START,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_ERROR,
} from "@/constants/action-types";

import api from "@/api/profile";

export const getProfile = id => {
  return dispatch => {
    dispatch(getProfileStart());

    api.getProfile(id)
      .then(response => {
        dispatch(getProfileSuccess(response));
      }, e => {
        dispatch(getProfileError(e));
      });
  }
};

export const getProfileStart = payload => {
  return { type: GET_PROFILE_START, payload }
};

export const getProfileSuccess = payload => {
  return { type: GET_PROFILE_SUCCESS, payload }
};

export const getProfileError = payload => {
  return { type: GET_PROFILE_ERROR, payload }
};

export default getProfile;