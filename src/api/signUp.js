import axiosInstance from "@/axiosInstance";

export default {
  signUpUser(payload) {
    return new Promise((resolve, reject) => {
      axiosInstance
        .post("/users", payload)
        .then(response => {
          if (response.data && response.data.success) {
            resolve(response.data);
          }
        })
        .catch(error => {
          reject(error.response.data);
        });
    });
  },
};