import axiosInstance from "@/axiosInstance";

export default {
  getPositions() {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get("/positions")
        .then(response => {
          if (response.data && response.data.success) {
            resolve(response.data);
          }
        })
        .catch(error => {
          reject(error.response.data);
        });
    });
  },
};