import axiosInstance from "@/axiosInstance";

export default {
  refreshToken() {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get("/token")
        .then(response => {
          if (response.data && response.data.success) {
            resolve(response.data);
          }
        })
        .catch(error => {
          reject(error.response.data);
        });
    });
  },
};