import axiosInstance from "@/axiosInstance";

export default {
  getProfile(id) {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get(`/users/${id}`)
        .then(response => {
          if (response.data && response.data.success) {
            resolve(response.data);
          }
        })
        .catch(error => {
          reject(error.response.data);
        });
    });
  },
};