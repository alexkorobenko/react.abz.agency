import axiosInstance from "@/axiosInstance";

export default {
  getUsers(url) {
    return new Promise((resolve, reject) => {
      axiosInstance
        .get(url || "/users?page=1&count=6")
        .then(response => {
          if (response.data && response.data.success) {
            resolve(response.data);
          }
        })
        .catch(error => {
          reject(error.response.data);
        });
    });
  },
};