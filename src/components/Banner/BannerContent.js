import React from "react";
import PropTypes from "prop-types";
import { ScrollTo } from "@/ui";

const BannerContent = ({ title, description, button }) => {
  return (
    <section className="section-banner__micro">
      <article>
        <h1>{title}</h1>
        {description &&
        <div dangerouslySetInnerHTML={{__html: description}}>
        </div>}
      </article>

      {button &&
      <footer>
        <ScrollTo {...button} />
      </footer>}
    </section>
  );
};

BannerContent.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  button: PropTypes.object,
};

export default BannerContent;