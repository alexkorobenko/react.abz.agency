import React from "react";
import PropTypes from "prop-types";
import BannerContent from "@/components/Banner/BannerContent";
import { ResponsiveBg } from "@/components";
import "./index.scss";

const Banner = (props) => {
  return (
    <section className="section-banner section-bg">
      {props.bg && <ResponsiveBg data={props.bg} />}
      <div className="container">
        <div className="row">
          <div className="col-12 col-md-7 col-lg-6">
            <BannerContent {...props} />
          </div>
        </div>
      </div>
    </section>
  );
};

Banner.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  bg: PropTypes.object,
  button: PropTypes.object,
};

export default Banner;