import React from "react";
import PropTypes from "prop-types";
import RelationshipsList from "@/components/Relationships/RelationshipsList";
import "./index.scss";

const Relationships = ({ title, items }) => {
  return (
    <section id="relationships"
             className="section-relationships section">
      <div className="container">
        <div className="section-relationships__row row justify-content-center">
          <div className="col-12 col-md-9 col-lg-12 text-center">
            <h2>{title}</h2>
          </div>
        </div>
        <RelationshipsList items={items} />
      </div>
    </section>
  );
};

Relationships.propTypes = {
  title: PropTypes.string.isRequired,
  items: PropTypes.array,
};

export default Relationships;