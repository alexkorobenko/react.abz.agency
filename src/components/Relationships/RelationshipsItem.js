import React from "react";
import PropTypes from "prop-types";
import "./RelationshipsItem.scss";

const RelationshipsItem = ({ title, description, image }) => {
  const picture = image ? require(`assets/images/Relationships/${image}`) : "";

  return (
    <div className="relationships-item">
      <div className="row">
        <div className="relationships-item__column col-12 col-md-2 col-lg-12">
          {picture &&
          <figure className="relationships-item__image">
            <img src={picture}
                 width="100"
                 height="115"
                 alt={title}
                 title={title}/>
          </figure>}
        </div>
        <div className="relationships-item__column col-12 col-md-10 col-lg-12">
          <div className="relationships-item__title">{title}</div>

          {description &&
          <article dangerouslySetInnerHTML={{__html: description}}
                   className="relationships-item__article">
          </article>}
        </div>
      </div>
    </div>
  );
};

RelationshipsItem.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  image: PropTypes.string,
};

export default RelationshipsItem;