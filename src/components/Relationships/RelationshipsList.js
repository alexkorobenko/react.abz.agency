import React from "react";
import PropTypes from "prop-types";
import RelationshipsItem from "@/components/Relationships/RelationshipsItem";

const RelationshipsList = ({ items }) => {
  const list = items.map((el, key) =>
    <div key={key}
         className="section-relationships__column col-12 col-lg-4">
      <RelationshipsItem {...el} />
    </div>
  );

  return (
    <>
      {items.length > 0 &&
      <div className="section-relationships__row row">
        {list}
      </div>}
    </>
  );
};

RelationshipsList.propTypes = {
  items: PropTypes.array,
};

RelationshipsList.defaultProps = {
  items: [],
};

export default RelationshipsList;