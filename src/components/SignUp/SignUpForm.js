import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getUsers, signUpUser } from "@/actions";
import { FormInput, FormSelect, FormImageLoader, Button, Notification } from "@/ui";
import { foreachObj } from "@/utils/helpers";
import classNames from 'classnames';
import "./SignUpForm.scss";

const baseClassName = "sign-up-form";

const mapStateToProps = state => {
  return {
    positionsState: state.positions,
    signUpState: state.signUp,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetUsers: url => (dispatch(getUsers(url))),

    onSignUpUser: (payload, callback) => {
      dispatch(
        signUpUser(payload)
      ).then(() => {
        (callback && callback.success) && callback.success();
      }, () => {
        (callback && callback.error) && callback.error();
      });
    },
  }
};

class SignUpForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validForm: null,
      fields: this.props.fields,
      notification: {
        title: null,
        message: null,
        visible: false,
      },
    };
  };

  static mergeFields(key, data, prevState) {
    return {
      ...prevState,
      [key]: {
        ...prevState[key],
        options: {
          ...prevState[key].options,
          ...data,
        },
      },
    };
  };

  static filterObjectByField(obj, fieldName, fieldValue) {
    const arr = [];

    foreachObj(obj, (data, key) => data[fieldName] === fieldValue && arr.push([key, data]));

    return arr;
  };

  onChangeHandler = (e) => {
    let name, value = null;

    if (e.target) {
      name = e.target.getAttribute("name");
      value = e.target.value;
    } else if (e.name && e.value) {
      name = e.name;
      value = e.value;
    }

    const valid = this.validateTest(name, value);
    const data = {
      value: value,
      valid: valid,
      touched: true,
    };

    this.setState(prevState => {
      return {
        fields: this.constructor.mergeFields(name, data, prevState.fields),
      }
    });
  };

  onSubmitHandler = (e) => {
    e.preventDefault();

    const validForm = this.validateForm();

    if (!validForm) return;

    let payload = new FormData();

    const fields = this.state.fields;

    foreachObj(fields, (field, key) => {
      let {value} = field.options;

      if (key === "phone") {
        value = value.replace(/[ ()]/g, "");
      }

      payload.append(key, value);
    });

    this.props.onSignUpUser(payload, {
      success: () => this.onSendHandler("success"),
      error: 	 () => this.onSendHandler("error"),
    });
  };

  validateTest(name, value) {
    if (!name) return false;
    const field = this.state.fields[name];
    if (!field) return false;

    if (field.type === "file") {
      return typeof value === "object" && value !== null;
    } else {
      return value !== undefined && value !== null && field.pattern.test(value);
    }
  };

  validateForm() {
    let validForm = true;
    let fields = this.state.fields;

    foreachObj(this.state.fields, (field, key) => {
      const {value} = field.options;
      const valid = this.validateTest(key, value);
      let data = {
        value: value,
        valid: valid,
        touched: true,
      };

      fields = this.constructor.mergeFields(key, data, fields);

      if (!valid) {
        validForm = false;
      }
    });

    this.setState({
      validForm: validForm,
      fields: fields,
    });

    return validForm;
  };

  resetForm() {
    let fields = this.state.fields;

    foreachObj(this.state.fields, (field, key) => {
      const data = {
        value: (key === "photo") ? null : "",
        valid: null,
        touched: false,
      };

      fields = this.constructor.mergeFields(key, data, fields);
    });

    this.setState({
      fields: fields,
      notification: {
        title: null,
        message: null,
        visible: false,
      },
    });
  };

  renderInputs() {
    const inputs = this.constructor.filterObjectByField(this.state.fields,"type", "input");
    const sortInputs = inputs.sort((a, b) => a[1]["priority"] - b[1]["priority"]);

    return (
      sortInputs.map(arr => {
        const key = arr[0];
        const data = arr[1];

        return (
          <div key={key}
               className={`${baseClassName}__column col-12 col-md-4`}>
            <FormInput name={key}
                       {...data.options}
                       onChange={this.onChangeHandler} />
          </div>
        );
      })
    );
  };

  renderPositionSelect() {
    const key = "position_id";
    const field = this.state.fields[key];
    const {positionsState} = this.props;

    return (
      <div>
        {positionsState.data.length &&
        <FormSelect name={key}
                    items={positionsState.data}
                    {...field.options}
                    onChange={this.onChangeHandler} />
        }
      </div>
    );
  };

  renderImageLoader() {
    const key = "photo";
    const field = this.state.fields[key];

    return (
      <FormImageLoader name={key}
                       {...field.options}
                       onChange={this.onChangeHandler} />
    );
  };

  onConfirmNotificationHandler = () => {
    this.resetForm();
  };

  onSendHandler(status) {
    let title = "";
    let {message, fails} = this.props.signUpState;
    if (!message) message = "";

    if (status === "success") {
      title = "Congratulations";
      this.props.onGetUsers(); //update users list

    } else if (status === "error") {
      title = "Error";
      foreachObj(fails, (value, key) => {
        message += `<br>${value.join(";")}`;
      });
    }

    this.setState({
      notification: {
        title: title,
        message: message,
        visible: true,
      },
    });
  };

  render() {
    const {button} = this.props;

    return (
      <form onSubmit={this.onSubmitHandler}
            className={classNames(baseClassName, {
              "disabled": this.props.signUpState.loading,
            })}>
        <div className={`${baseClassName}__row row row--top`}>
          {this.renderInputs()}
        </div>
        <div className={`${baseClassName}__row row row--middle`}>
          <div className={`${baseClassName}__column col-12 col-md-6`}>
            {this.renderPositionSelect()}
          </div>
          <div className={`${baseClassName}__column col-12 col-md-6`}>
            {this.renderImageLoader()}
          </div>
        </div>
        <div className={`${baseClassName}__row row row--bottom`}>
          <div className={`${baseClassName}__column col-12`}>
            <Button type="submit"
                    {...button} />
          </div>
        </div>
        <Notification {...this.state.notification}
                      onConfirm={this.onConfirmNotificationHandler} />
      </form>
    );
  };
}

SignUpForm.propTypes = {
  fields: PropTypes.object,
  button: PropTypes.object,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUpForm);