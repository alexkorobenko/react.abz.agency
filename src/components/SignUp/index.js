import React from "react";
import PropTypes from "prop-types";
import SignUpForm from "@/components/SignUp/SignUpForm";
import "./index.scss";

const baseClassName = "section-sign-up";

const SignUp = ({ title, subTitle, fields, button }) => {
  return (
    <section id="sign-up"
             className={`${baseClassName} section`}>
      <div className={`${baseClassName}__mask`}>
      </div>
      <div className={`${baseClassName}__wrapper container`}>
        <div className={`${baseClassName}__row row justify-content-center`}>
          <div className={`${baseClassName}__header text-center col-12 col-md-8 col-lg-12`}>
            <h2>{title}</h2>
            {subTitle && <h5>{subTitle}</h5>}
          </div>
        </div>
        <div className={`${baseClassName}__row`}>
          <SignUpForm fields={fields}
                      button={button} />
        </div>
      </div>
    </section>
  );
};

SignUp.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
  fields: PropTypes.object,
  button: PropTypes.object,
};

export default SignUp;