import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import ResponsiveImages from "@/utils/ResponsiveImages";
import classNames from "classnames";
import styles from "./index.module.scss";

const host = window["location"].origin;

const ResponsiveBg = ({ className, data }) => {
  const ref = useRef();

  useEffect(() => {
    if (!window["responsiveImagesObserver"]) return;

    const observer = new ResponsiveImages(ref.current);

    window["responsiveImagesObserver"].subscribe(observer);

    return () => {
      window["responsiveImagesObserver"].unsubscribe(observer);
    }
  }, []); // eslint-disable-line

  return (
    <div ref={ref}
         className={classNames(styles.section, className)}>
      <div className={classNames(styles.item, "js-responsive-image")}
           data-xl-src={`${host}/${data.xl}`}
           data-lg-src={`${host}/${data.lg}`}
           data-md-src={`${host}/${data.md}`}
           data-sm-src={`${host}/${data.sm}`}
           data-xs-src={`${host}/${data.xs}`}>
      </div>
    </div>
  );
};

ResponsiveBg.propTypes = {
  className: PropTypes.string,
  data: PropTypes.shape({
    xl: PropTypes.string,
    lg: PropTypes.string,
    md: PropTypes.string,
    sm: PropTypes.string,
    xs: PropTypes.string,
  }).isRequired,
};

ResponsiveBg.defaultProps = {
  className: "",
};

export default ResponsiveBg;