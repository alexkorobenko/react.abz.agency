import React from "react";
import PropTypes from "prop-types";
import { Link, ScrollTo } from "@/ui";

const Menu = ({ prefix, items }) => {
  const itemClassName = `${prefix}__button`;
  const list = items.map((el, key) =>
    <li key={key}
        className={`${prefix}__item`}>
      {el.action && el.action === "scrollTo" ?
        <ScrollTo title={el.title}
                  dataHref={el.href}
                  className={itemClassName} /> :
        <Link title={el.title}
              href={el.href}
              className={itemClassName} />
      }
    </li>
  );

  return (
    <>
      {items.length > 0 &&
      <ul className={prefix}>
        {list}
      </ul>}
    </>
  );
};

Menu.propTypes = {
  prefix: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.shape({
    "title": PropTypes.string,
    "href": PropTypes.string,
    "action": PropTypes.string,
  })),
};

Menu.defaultProps = {
  prefix: "menu",
  items: [],
};

export default Menu;