import React from "react";
import PropTypes from "prop-types";
import AboutContent from "@/components/About/AboutContent";
import "./index.scss";

const About = (props) => {
  return (
    <section id="about-me"
             className="section-about section">
      <div className="container">
        <div className="section-about__row row">
          <div className="col-12 col-md-4 d-none d-md-block section-about__column">
          </div>
          <div className="col-12 col-md-8 section-about__column">
            <h2>{props.title}</h2>
          </div>
        </div>
        <AboutContent {...props} />
      </div>
    </section>
  );
};

About.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  image: PropTypes.string,
  button: PropTypes.object,
};

export default About;