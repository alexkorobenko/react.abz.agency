import React from "react";
import PropTypes from "prop-types";
import { ScrollTo } from "@/ui";

const AboutContent = ({ title, description, image, button }) => {
  const picture = image ? require(`assets/images/About/${image}`) : "";

  return (
    <div className="section-about__row row">
      <div className="col-12 col-md-4 section-about__column">
        {picture &&
        <figure className="section-about__image">
          <img src={picture}
               width="289"
               height="285"
               alt={title}
               title={title} />
        </figure>}
      </div>
      <div className="col-12 col-md-8 section-about__column">
        <div className="section-about__content">
          {description &&
          <article className="article"
                   dangerouslySetInnerHTML={{__html: description}}>
          </article>}

          {button &&
          <footer>
            <ScrollTo {...button} />
          </footer>}
        </div>
      </div>
    </div>
  );
};

AboutContent.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  image: PropTypes.string,
  button: PropTypes.object,
};

export default AboutContent;