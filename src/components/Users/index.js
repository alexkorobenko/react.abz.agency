import React, { useCallback, useEffect } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { getUsers } from "@/actions/users";
import UsersList from "@/components/Users/UsersList";
import "./index.scss";

const Users = ({ title, subTitle, button }) => {
  const usersState = useSelector(({users}) => users);
  const dispatch = useDispatch();

  const onGetUsers = url => dispatch(getUsers(url));

  const onLoadUsersHandler = useCallback(() => {
    const {nextUrl} = usersState;
    nextUrl && onGetUsers(nextUrl);
  },[usersState]); // eslint-disable-line

  useEffect(() => {
    onGetUsers()
  }, []); // eslint-disable-line

  return (
    <section id="users"
             className="section-users section">
      <div className="section-users__container container">
        <div className="section-users__row row justify-content-center">
          <div className="col-12 col-md-8 col-lg-12">
            <div className="section-users__header">
              <h2>{title}</h2>
              {subTitle && <h5>{subTitle}</h5>}
            </div>
          </div>
        </div>
        <div className="section-users__row row">
          <UsersList {...usersState}
                     button={button}
                     onLoadUsers={onLoadUsersHandler} />
        </div>
      </div>
    </section>
  );
};

Users.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string,
  button: PropTypes.object,
};

export default Users;