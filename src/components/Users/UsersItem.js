import React from "react";
import PropTypes from "prop-types";
import "./UsersItem.scss";

const parsePhone = value => {
  value = value.split(" ").join("");
  return value.substr(0, 3) + " (" + value.substr(3, 3) + ") " + value.substr(6, 3) + " " + value.substr(9, 2) + " " + value.substr(11, 2);
};

const UsersItem = ({ photo, name, email, phone, position }) => {
  return (
    <div className="users-item">
      <div className="users-item__image"
           style={{ backgroundImage: `url(${photo})` }}>
      </div>
      <div className="users-item__title">{name}</div>
      <div className="users-item__description">
        {position && <p>{position}</p>}
        {email && <p><a href={"mailto:" + email}>{email}</a></p>}
        {phone && <p><a href={"tel:" + phone}>{parsePhone(phone)}</a></p>}
      </div>
    </div>
  );
};

UsersItem.propTypes = {
  photo: PropTypes.string,
  name: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  position: PropTypes.string,
};

export default UsersItem;