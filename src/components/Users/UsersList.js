import React from "react";
import PropTypes from "prop-types";
import UsersItem from "@/components/Users/UsersItem";
import Button from "@/ui/Button";
import "./UsersList.scss";

const UsersList = ({ loading, items, button, nextUrl, error, onLoadUsers }) => {
  const sortingList = items.sort((a, b)  => b["registration_timestamp"] - a["registration_timestamp"]);

  const list = sortingList.map((el, key) =>
    <div key={key}
         className="users-list__column col-12 col-md-4">
      <UsersItem {...el} />
    </div>
  );

  return (
    <div className="users-list">
      {(list.length > 0) &&
      <div className="users-list__body">
        <div className="users-list__row row">
          {list}
        </div>
      </div>}

      {(button && nextUrl) &&
      <div className="users-list__footer">
        <Button {...button}
                disabled={loading}
                onClick={onLoadUsers} />
      </div>}

      {error && <p>{error}</p>}
    </div>
  );
};

UsersList.propTypes = {
  loading: PropTypes.bool,
  items: PropTypes.array,
  button: PropTypes.object,
  nextUrl: PropTypes.string,
  error: PropTypes.string,
  onLoadUsers: PropTypes.func
};

export default UsersList;