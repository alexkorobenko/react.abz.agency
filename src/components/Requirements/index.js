import React from "react";
import PropTypes from "prop-types";
import RequirementsContent from "@/components/Requirements/RequirementsContent";
import { ResponsiveBg } from "@/components";
import "./index.scss";

const Requirements = (props) => {
  return (
    <section id="requirements"
             className="section-requirements section-bg">
      {props.bg && <ResponsiveBg data={props.bg} />}
      <div className="container">
        <div className="section-requirements__row row justify-content-center">
          <div className="col-12 col-md-8 col-lg-12 text-center">
            <h2>{props.title}</h2>
          </div>
        </div>
        <RequirementsContent {...props} />
      </div>
    </section>
  );
};

Requirements.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  image: PropTypes.string,
  bg: PropTypes.object,
};

export default Requirements;