import React from "react";
import PropTypes from "prop-types";

const RequirementsContent = ({ title, description, image }) => {
  const picture = image ? require(`assets/images/Requirements/${image}`) : "";

  return (
    <div className="section-requirements__row row">
      <div className="section-requirements__column col-12 col-md-6">
        {description &&
        <article dangerouslySetInnerHTML={{__html: description}}
                 className="article">
        </article>}
      </div>
      <div className="section-requirements__column col-12 col-md-6">
        {picture &&
        <figure className="section-requirements__image">
          <img src={picture}
               width="430"
               height="360"
               alt={title}
               title={title} />
        </figure>}
      </div>
    </div>
  );
};

RequirementsContent.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  image: PropTypes.string,
};

export default RequirementsContent;