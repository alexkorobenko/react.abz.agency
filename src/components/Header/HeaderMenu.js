import React from "react";
import PropTypes from "prop-types";
import { Menu } from "@/components";
import "./HeaderMenu.scss";

const HeaderMenu = ({ menu }) => {
  return (
    <Menu prefix="header-menu"
          items={menu} />
  );
};

HeaderMenu.propTypes = {
  menu: PropTypes.array,
};

export default HeaderMenu;