import React from "react";
import PropTypes from "prop-types";
import HeaderLogo from "@/components/Header/HeaderLogo";
import { Button } from "@/ui";

const HeaderMobile = ({ navOnClickHandler }) => {
  return (
    <div className="header__mobile">
      <div className="row header__row">
        <div className="col-6 header__col col--left">
          <HeaderLogo />
        </div>
        <div className="col-6 header__col col--right">
          <Button onClick={navOnClickHandler}
                  className="button--nav">
            <svg width="24" height="22" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M2.9 3.85C2.40074 3.8635 1.91724 3.67457 1.55941 3.32615C1.20157 2.97773 0.999814 2.49944 0.999997 2C0.972537 1.479 1.16053 0.969627 1.51986 0.591376C1.8792 0.213125 2.37827 -0.000721312 2.9 1.82808e-06H22.05C23.135 0.0271351 24.0003 0.914668 24 2C24.0002 2.5127 23.7931 3.0037 23.4259 3.3615C23.0587 3.71929 22.5625 3.91349 22.05 3.9L2.9 3.85Z" fill="#283593"/>
              <path d="M22.05 8.57001C23.1159 8.59634 23.9737 9.45415 24 10.52C24.0002 11.0327 23.7931 11.5237 23.4259 11.8815C23.0587 12.2393 22.5625 12.4335 22.05 12.42H2.89999C2.39194 12.4339 1.90053 12.2382 1.54115 11.8788C1.18177 11.5195 0.986067 11.0281 0.999994 10.52C0.986502 10.0075 1.18071 9.51127 1.5385 9.14407C1.89629 8.77686 2.3873 8.56983 2.89999 8.57001H22.05Z" fill="#283593"/>
              <path d="M22.05 17.2C22.5608 17.1865 23.0555 17.3794 23.4224 17.735C23.7893 18.0906 23.9975 18.579 24 19.09C24.0003 20.1753 23.135 21.0629 22.05 21.09H2.9C2.37827 21.0907 1.8792 20.8769 1.51986 20.4986C1.16053 20.1204 0.972537 19.611 0.999997 19.09C0.988858 18.5837 1.18579 18.0949 1.54486 17.7377C1.90392 17.3805 2.39372 17.1862 2.9 17.2H22.05Z" fill="#283593"/>
            </svg>
          </Button>
        </div>
      </div>
    </div>
  );
};

HeaderMobile.propTypes = {
  navOnClickHandler: PropTypes.func,
};

export default HeaderMobile;