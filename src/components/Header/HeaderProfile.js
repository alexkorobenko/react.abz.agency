import React from "react";
import { useSelector } from "react-redux";
import { Button } from "@/ui";
import classNames from "classnames";
import "./HeaderProfile.scss";

const HeaderProfile = () => {
  const profile = useSelector(({profile}) => profile);
  const {loading, error} = profile;
  const {name, email, photo} = profile.data;

  return (
    <>
      {error ?
        <p>{error}</p> :
        <div className={classNames("header-profile", {
          "loading": loading,
        })}>
          <div className="header-profile__column column--left">
            <div className="header-profile__name">{name}</div>
            <div className="header-profile__email">{email}</div>
          </div>
          <div className="header-profile__column column--center">
            <div className="header-profile__image"
                 style={{ backgroundImage: `url(${photo})` }}>
            </div>
          </div>
          <div className="header-profile__column column--right">
            <Button className="button--sign-out">
              <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M9.79999 18.12C9.7779 18.0552 9.7779 17.9848 9.79999 17.92C9.79999 17.86 9.79999 17.81 9.68999 17.78C9.57999 17.75 9.56999 17.71 9.56999 17.69C9.56999 17.67 9.50999 17.69 9.38999 17.69H9.20999H4.40999C3.75844 17.7002 3.13189 17.4395 2.67999 16.97C2.2095 16.4969 1.96227 15.8461 1.99999 15.18V4.41C1.98976 3.75845 2.25052 3.1319 2.71999 2.68C3.16899 2.23533 3.77815 1.99022 4.40999 2H9.30999C9.42219 2.00655 9.53277 1.97087 9.61999 1.9C9.70444 1.82129 9.75769 1.71478 9.76999 1.6C9.7951 1.48132 9.81181 1.36103 9.81999 1.24C9.81999 1.13 9.81999 0.999999 9.81999 0.839999C9.81999 0.679999 9.81999 0.569999 9.81999 0.529999C9.82884 0.38316 9.76536 0.241265 9.64999 0.149999C9.5628 0.0544082 9.43938 -4.19659e-05 9.30999 -5.99422e-07H4.40999C3.23694 -0.0168658 2.10862 0.449651 1.28999 1.29C0.449644 2.10863 -0.0168725 3.23695 -7.27217e-06 4.41V15.18C-0.0192575 16.3535 0.447634 17.4827 1.28999 18.3C2.10862 19.1403 3.23694 19.6069 4.40999 19.59H9.30999C9.42219 19.5965 9.53277 19.5609 9.61999 19.49C9.70854 19.4144 9.76266 19.3062 9.76999 19.19C9.7951 19.0713 9.81181 18.951 9.81999 18.83C9.81999 18.73 9.81999 18.59 9.81999 18.43C9.81999 18.27 9.79999 18.16 9.79999 18.12Z" fill="#283593"/>
                <path d="M23.71 9.11001L15.38 0.780005C14.9937 0.411751 14.3863 0.411751 14 0.780005C13.8062 0.956732 13.6971 1.20776 13.7 1.47001V5.88001H6.85999C6.3077 5.88001 5.85999 6.32772 5.85999 6.88001V12.75C5.85999 13.3023 6.3077 13.75 6.85999 13.75H13.71V18.16C13.7071 18.4223 13.8162 18.6733 14.01 18.85C14.3949 19.2119 14.995 19.2119 15.38 18.85L23.71 10.52C24.0719 10.1351 24.0719 9.53496 23.71 9.15001V9.11001Z" fill="#283593"/>
              </svg>
            </Button>
          </div>
        </div>
      }
    </>
  );
};

export default HeaderProfile;