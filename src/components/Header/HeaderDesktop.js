import React, { useEffect, useRef } from "react";
import PropTypes from "prop-types";
import HeaderLogo from "@/components/Header/HeaderLogo";
import HeaderMenu from "@/components/Header/HeaderMenu";
import HeaderProfile from "@/components/Header/HeaderProfile";

const HeaderDesktop = ({ menu, onNavContentRef }) => {
  const navContentRef = useRef();

  useEffect(() => {
    onNavContentRef(navContentRef);
  }, [onNavContentRef]);

  return (
    <div className="header__desktop">
      <div ref={navContentRef}
           className="header__desktop-content">
        <div className="row header__row">
          <div className="col-2 header__col col--left">
            <HeaderLogo />
          </div>
          <div className="col-7 header__col col--center">
            <HeaderMenu menu={menu} />
          </div>
          <div className="col-3 header__col col--right">
            <HeaderProfile />
          </div>
        </div>
      </div>
    </div>
  );
};

HeaderDesktop.propTypes = {
  menu: PropTypes.array,
};

export default HeaderDesktop;