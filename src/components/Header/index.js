import React, { useState } from "react";
import PropTypes from "prop-types";
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock/lib/bodyScrollLock.es6";
import HeaderDesktop from "@/components/Header/HeaderDesktop";
import HeaderMobile from "@/components/Header/HeaderMobile";
import classNames from "classnames";
import "./index.scss";

const Header = ({ menu }) => {
  let navContentRef = {
    current: null,
  };
  const [navVisible, setNavVisible] = useState(false);

  const setNavContentRef = (ref) => (navContentRef = ref);

  const toggleNav = () => {
    setNavVisible(!navVisible);
    !navVisible ? disableBodyScroll(navContentRef.current) : enableBodyScroll(navContentRef.current);
  };

  return (
    <header className={classNames("header", {
      "header-nav--visible": navVisible,
    })}>
      <div className="header__wrapper">
        <div className="container">
          <HeaderDesktop menu={menu}
                         onNavContentRef={setNavContentRef} />
          <HeaderMobile navOnClickHandler={toggleNav} />
        </div>
        <div onClick={toggleNav}
             className="header__mask">
        </div>
      </div>
    </header>
  );
};

Header.propTypes = {
  menu: PropTypes.array,
};

export default Header;