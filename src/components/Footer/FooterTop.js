import React from "react";
import PropTypes from "prop-types";
import FooterLogo from "@/components/Footer/FooterLogo";
import FooterMenu from "@/components/Footer/FooterMenu";

const FooterTop = ({ menu }) => {
  return (
    <div className="footer__row row row--top">
      <div className="footer__column column--left col-12 col-md-3">
        <FooterLogo />
      </div>
      <div className="footer__column column--right col-12 col-md-9">
        <FooterMenu menu={menu} />
      </div>
    </div>
  );
};

FooterTop.propTypes = {
  menu: PropTypes.array,
};

export default FooterTop;