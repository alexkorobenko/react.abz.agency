import React from "react";
import PropTypes from "prop-types";
import FooterSocial from "@/components/Footer/FooterSocial";

const FooterBottom = ({ social, copyright }) => {
  return (
    <div className="footer__row row row--bottom">
      <div className="footer__column column--left col-12 col-md-4">
        {copyright &&
        <p className="footer__copyright"
           dangerouslySetInnerHTML={{__html: copyright}}>
        </p>}
      </div>
      <div className="footer__column column--right col-12 col-md-8">
        <FooterSocial data={social} />
      </div>
    </div>
  );
};

FooterBottom.propTypes = {
  social: PropTypes.array,
  copyright: PropTypes.string,
};

export default FooterBottom;