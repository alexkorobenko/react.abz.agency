import React from "react";
import PropTypes from "prop-types";
import { Link } from "@/ui";
import "./FooterContact.scss";

const prefix = "footer-contact";

const icon = type => (
  <span className="button--icon">
			{
        {
          "mail": (
            <svg width="24" height="18" viewBox="0 0 24 18" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M12 12L9.00001 9.39999L0.540009 16.68C0.866211 16.9751 1.29013 17.139 1.73001 17.14H22.27C22.7073 17.1413 23.129 16.977 23.45 16.68L15 9.39999L12 12Z" fill="white"/>
              <path d="M23.46 0.459997C23.1338 0.164893 22.7099 0.00102462 22.27 -2.86241e-06H1.72999C1.2907 -0.00167939 0.867822 0.166756 0.549988 0.469997L12 10.29L23.46 0.459997Z" fill="white"/>
              <path d="M0 1.5V15.75L8.29 8.71L0 1.5Z" fill="white"/>
              <path d="M15.71 8.71L24 15.75V1.5L15.71 8.71Z" fill="white"/>
            </svg>
          ),
          "phone": (
            <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M4.99999 9.66999C6.63874 12.8036 9.1964 15.3612 12.33 17L14.78 14.56C15.0448 14.2235 15.5133 14.1264 15.89 14.33C17.1729 14.7894 18.5274 15.0162 19.89 15C20.1939 14.9642 20.4975 15.0696 20.7139 15.286C20.9304 15.5024 21.0358 15.806 21 16.11V19.89C21.0358 20.1939 20.9304 20.4975 20.7139 20.7139C20.4975 20.9304 20.1939 21.0358 19.89 21C14.8784 21.0053 10.0706 19.0168 6.52688 15.4731C2.98316 11.9294 0.994677 7.12156 0.999991 2.10999C0.964229 1.80604 1.06962 1.50244 1.28603 1.28603C1.50244 1.06962 1.80604 0.964229 2.10999 0.999991H5.99999C6.30394 0.964229 6.60754 1.06962 6.82395 1.28603C7.04036 1.50244 7.14575 1.80604 7.10999 2.10999C7.11301 3.471 7.33935 4.82229 7.77999 6.10999C7.8687 6.49443 7.78863 6.89845 7.55999 7.21999L4.99999 9.66999Z" fill="white"/>
            </svg>
          ),
          "mobile": (
            <svg width="16" height="28" viewBox="0 0 16 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.54 27.12C14.1983 27.1339 14.8337 26.8777 15.2983 26.4112C15.763 25.9446 16.0165 25.3083 16 24.65V2.46999C16.0165 1.81172 15.763 1.17534 15.2983 0.708784C14.8337 0.242226 14.1983 -0.0138831 13.54 -1.39454e-05H2.46C1.80167 -0.0138831 1.16633 0.242226 0.701658 0.708784C0.236988 1.17534 -0.0165413 1.81172 -1.53587e-06 2.46999V24.65C-0.0165413 25.3083 0.236988 25.9446 0.701658 26.4112C1.16633 26.8777 1.80167 27.1339 2.46 27.12H13.54ZM8 26.09C7.32069 26.09 6.77 25.5393 6.77 24.86C6.77 24.1807 7.32069 23.63 8 23.63C8.67931 23.63 9.23 24.1807 9.23 24.86C9.23 25.5393 8.67931 26.09 8 26.09ZM4.92 1.49999C4.91995 1.42661 4.94976 1.35638 5.00258 1.30544C5.0554 1.25451 5.12667 1.22727 5.2 1.22999H10.8C10.9508 1.22989 11.0746 1.34927 11.08 1.49999V1.57999C11.08 1.65336 11.0502 1.7236 10.9974 1.77453C10.9446 1.82546 10.8733 1.8527 10.8 1.84999H5.2C5.04918 1.85008 4.92538 1.73071 4.92 1.57999V1.49999ZM1.23 3.07999H14.77V22.77H1.23V3.07999Z" fill="white"/>
            </svg>
          ),
        }[type]
      }
		</span>
);

const item = data => {
  let href = "";

  switch (data.type) {
    case "mail":
      href += "mailto:";
      break;

    case "phone":
      href += "tel:";
      break;

    case "mobile":
      href += "tel:";
      break;

    default:
      href += "";
  }

  href += data.href;

  return (
    <Link href={href}
          className={`${prefix}__button`}>
      {icon(data.type)}
      <span className="button--title">{data.title}</span>
    </Link>
  );
};

const FooterContact = ({ items }) => {
  const list = items.map((el, key) =>
    <li key={key}
        className={`${prefix}__item`}>
      {item(el)}
    </li>
  );

  return (
    <>
      {items.length > 0 &&
      <ul className={prefix}>
        {list}
      </ul>
      }
    </>
  );
};

FooterContact.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    "type": PropTypes.string,
    "href": PropTypes.string,
    "title": PropTypes.string,
  })),
};

FooterContact.defaultProps = {
  items: [],
};

export default FooterContact;