import React from "react";
import PropTypes from "prop-types";
import FooterContact from "@/components/Footer/FooterContact";
import FooterSiteMenu from "@/components/Footer/FooterSiteMenu";

const FooterMiddle = ({ menu, contact }) => {
  return (
    <div className="footer__row row row--middle">
      <div className="footer__column column--left col-12 col-md-5">
        <FooterContact items={contact} />
      </div>
      <div className="footer__column column--right col-12 col-md-7 d-none d-md-block">
        <FooterSiteMenu menu={menu} />
      </div>
    </div>
  );
};

FooterMiddle.propTypes = {
  contact: PropTypes.array,
  menu: PropTypes.array,
};

export default FooterMiddle;