import React from "react";
import PropTypes from "prop-types";
import { Menu } from "@/components";
import "./FooterMenu.scss";

const FooterMenu = ({ menu }) => {
  return (
    <Menu prefix="footer-menu"
          items={menu} />
  );
};

FooterMenu.propTypes = {
  menu: PropTypes.array,
};

export default FooterMenu;