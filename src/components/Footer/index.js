import React from "react";
import PropTypes from "prop-types";
import FooterTop from "@/components/Footer/FooterTop";
import FooterMiddle from "@/components/Footer/FooterMiddle";
import FooterBottom from "@/components/Footer/FooterBottom";
import "./index.scss";

const Footer = ({ homeMenu, siteMenu, contact, copyright, social }) => {
  return (
    <footer className="footer">
      <div className="footer__wrapper">
        <div className="container">
          <FooterTop menu={homeMenu} />
          <FooterMiddle menu={siteMenu}
                        contact={contact} />
          <FooterBottom copyright={copyright}
                        social={social} />
        </div>
      </div>
    </footer>
  );
};

Footer.propTypes = {
  homeMenu: PropTypes.array,
  siteMenu: PropTypes.array,
  contact: PropTypes.array,
  social: PropTypes.array,
  copyright: PropTypes.string,
};

export default Footer;