import React from "react";
import PropTypes from "prop-types";
import { Menu } from "@/components";
import "./FooterSiteMenu.scss";

const FooterSiteMenu = ({ menu }) => {
  return (
    <Menu prefix="footer-site-menu"
          items={menu} />
  );
};

FooterSiteMenu.propTypes = {
  menu: PropTypes.array,
};

export default FooterSiteMenu;