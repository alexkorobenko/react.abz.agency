import axios from "axios";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
});

const routesWithToken = ["/users"];

axiosInstance.defaults.headers.post["Content-Type"] = "application/json";

axiosInstance.interceptors.request.use(request => {
  if (routesWithToken.indexOf(request.url) !== -1) {
    request.headers.Token = window.localStorage.getItem("token");
  }

  return request;
}, error => {
  return Promise.reject(error);
});

axiosInstance.interceptors.response.use(response => {
  return response;
}, error => {
  return Promise.reject(error);
});

export default axiosInstance;